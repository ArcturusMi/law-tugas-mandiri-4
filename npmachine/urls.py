from django.urls import path
from npmachine.views import StudentPostView, StudentGetView

urlpatterns = [
    path('students/<str:npm>', StudentGetView.as_view(), name='url_get_sweet'),
    path('students/', StudentPostView.as_view(), name='url_post_sweet'),
]