from rest_framework import serializers


class StudentPostSerializer(serializers.Serializer):
    student_name = serializers.CharField(max_length=255)
    npm = serializers.CharField(max_length=255)
    
class StudentGetSerializer(StudentPostSerializer):
    status = serializers.CharField(max_length=255)
