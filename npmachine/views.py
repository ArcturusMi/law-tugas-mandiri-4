from urllib import response
from django.shortcuts import render
from django.http import FileResponse, HttpResponse
from rest_framework.response import Response
from rest_framework.views import APIView
import os
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings
from .models import *
from .serializers import *

# Create your views here.
class StudentPostView(APIView):
    def post(self, request, format=None):
        serializer = StudentPostSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        student = Student.objects.filter(npm=data["npm"]).first()
        if student:
            student.name = data["student_name"]
        else:
            student = Student(student_name=data["student_name"],npm=data["npm"])
        student.save()    

        return Response({'status':'OK'}, headers = {"Access-Control-Allow-Origin":"*"},status=200)
    

class StudentGetView(APIView):
    def get(self, request, npm, format=None):
        target_student = Student.objects.filter(npm=npm).first()
        if target_student:
            target_name = target_student.student_name
            target_npm = target_student.npm
            
            return Response({'status':'OK','name':target_name,'npm':target_npm}, headers = {"Access-Control-Allow-Origin":"*"},status=200)
        else:
            return Response({'status':'Not Found'}, headers = {"Access-Control-Allow-Origin":"*"},status=404)
        
            